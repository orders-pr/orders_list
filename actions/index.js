import { v4 as uuidv4 } from 'uuid';

let nextImage = 0

export function fetchOrders() {
  return dispatch => {
    dispatch(fetchOrdersBegin());
    return fetch("http://localhost:4567/orders")
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchOrdersSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchOrdersFailure(error)));
  };
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export const addImage= () => ({
  type: 'ADD_IMAGE',
  key: uuidv4(),
  title: `file ${nextImage}`,
  id: nextImage++,
  uri: null,
})

export const changeOrder = id => ({
  type: 'CHANGE_ORDER',
  id
})

export const fetchOrdersBegin = () => ({
  type: 'FETCH_ORDERS_BEGIN'
});

export const fetchOrdersSuccess = orders => ({
  type: 'FETCH_ORDERS_SUCCESS',
  payload: { orders }
});

export const fetchOrdersFailure = error => ({
  type: 'FETCH_ORDERS_FAILURE',
  payload: { error }
});
