const initialState = {
  id: 1
}

const order = ( state = initialState, action) => {
  switch (action.type) {
    case 'CHANGE_ORDER':
      state.id = action.id
    default:
      return state  
  }
}

export default order
