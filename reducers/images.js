import { v4 as uuidv4 } from 'uuid';

const initialImages = [
  {
    id: 1,
    key: uuidv4(),
    title: 'image_1',
    url: null
  },
  {
    id: 2,
    key: uuidv4(),
    title: 'image_2',
    url: null
  }
]

const images = (state = initialImages, action) => {
  switch (action.type) {
    case 'ADD_IMAGE':
      return [
        ...state,
        {
          id: action.id,
          key: action.key,
          title: action.title,
          url: action.url
        }
      ]
    default:
      return state
  }
}

export default images
