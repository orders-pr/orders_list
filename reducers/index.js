import { combineReducers } from 'redux'
import images from './images'
import order from './order'

export default combineReducers({
  images,
  order,
})
