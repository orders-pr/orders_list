import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import rootReducer from './reducers'

import HomeScreen from './screens/HomeScreen'
import FilesScreen from './screens/FilesScreen'
import OrdersScreen from './screens/OrdersScreen'

const store = createStore(rootReducer)

export default function App() {

  const Stack = createStackNavigator();

  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator >
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="Orders" component={OrdersScreen} />
          <Stack.Screen name="Files" component={FilesScreen} />
        </Stack.Navigator> 
      </NavigationContainer>
    </Provider>
  );
}
