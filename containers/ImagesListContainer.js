import NewList from '../components/NewList'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return { images: state.images }
}

export default connect(mapStateToProps)(NewList)
