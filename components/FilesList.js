import React, { Fragment, Component } from 'react';
import { ActivityIndicator, FlatList, Text, StyleSheet, View, Image } from 'react-native';

export default class FilesList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
    };
  }  

  componentDidMount() {
    fetch('http://localhost:4567/files')
      .then((response) => response.json())
      .then((json) => this.setState({ data: json.files}))
      .catch((error) => console.error(error))
      .finally(() => this.setState({ isLoading: false}));
  }

  render() { 
    return (
      <Fragment>
        <Text style={styles.title}>Files</Text>
        {this.state.isLoading ? <ActivityIndicator/> : (
              <FlatList
                data={Object.entries(this.state.data)}
                keyExtractor={item => item[0]}
                renderItem={({ item }) => (
                  <Fragment>
                    <Image style={styles.image} source={{ uri: 'http://localhost:4567/' + item[1] }}/>
                    <Text style={styles.text}> { item[1] } </Text>
                  </Fragment>  
                )}
              />
            )}
      </Fragment>
    );
  }

}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 24,
    paddingTop: 15,
    paddingBottom: 15,
  },
  image: {
    marginBottom: 10,
    width: 300,
    height: 200,
  },
  text: {
    marginBottom: 20,
    textAlign: 'center'
  }
})
