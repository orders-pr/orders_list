import React, { Component, Fragment } from 'react';
import { StyleSheet, TouchableHighlight, Image, View, Text } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';

export default class ImagePickerBlock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      data: [],
      image: null,
    };
  }  

  componentDidMount() {
    this.getPermissionAsync();
  }

  // componentDidUpdate() {
  //   this.props.updateData(this.state.image, this.props.dataKey)
  // }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  _pickImage = async () => {
    try {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
        base64: false,
      });
      if (!result.cancelled) {
        this.setState({ image: result.uri });
      }

      console.log(result);
    } catch (E) {
      console.log(E);
    }
  };

  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginBottom: 30 }}>
          <Text>{this.props.title}</Text>
          {this.state.image && <Image source={{ uri: this.state.image }} style={{ width: 300, height: 200 }} />}
          <View style={styles.row}>
            <TouchableHighlight onPress={this._pickImage}>
              <View style={styles.buttonSuccess}>
                <Text style={styles.buttonText}>Pick an image</Text>
              </View>   
            </TouchableHighlight>
            {/* <TouchableHighlight onPress={this.props.remove}>
              <View style={styles.buttonAllert}>
                <Text style={styles.buttonText}>Delete image</Text>
              </View>   
            </TouchableHighlight> */}
          </View>
      </View>
    );
  }
}

ImagePickerBlock.defaultProps = {
  title: 'file', 
  remove:() => {},
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    flexWrap: 'nowrap',
    width: '100%',
  },
  button: {
    width: 'calc(100% - 20px)',
    marginVertical: 20,
    marginHorizontal: 10,
    height: 50,
    alignItems: 'center',
    backgroundColor: '#2196F3',
    borderRadius: 2
  },
  buttonSuccess: {
    width: 'calc(100% - 20px)',
    marginVertical: 20,
    marginHorizontal: 10,
    height: 50,
    alignItems: 'center',
    backgroundColor: '#5DAA5B',
    borderRadius: 2
  },
  buttonAllert: {
    width: 'calc(100% - 20px)',
    marginVertical: 20,
    marginHorizontal: 10,
    height: 50,
    alignItems: 'center',
    backgroundColor: '#CF4F57',
    borderRadius: 2
  },
  buttonText: {
    textTransform: 'uppercase',
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    padding: 15,
    color: 'white'
  }
});
