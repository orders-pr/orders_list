import React from 'react'
import { FlatList } from 'react-native'
import ImagePickerBlock from './ImagePickerBlock'

const NewList = ({ images }) => (
  <FlatList
  data={ images }
  keyExtractor={ item => item.key}
  renderItem={({ item }) => (
    <ImagePickerBlock
      title={ item.title } 
      dataKey={ item.key }
    />
  )}
  /> 
)

export default NewList
