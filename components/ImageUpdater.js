import React, { Component, Fragment } from 'react'
import { StyleSheet, TouchableHighlight, View, Text } from 'react-native'
import ImagesList from './ImagesList'
import NewList from './NewList'
import { v4 as uuidv4 } from 'uuid'
import { connect } from 'react-redux'

import { buttonsStyles } from "../styles/button_style"

class ImageUpdater extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageChanged: [],
    }

    this.updateData = this.updateData.bind(this);
    this.removeImageBlock = this.removeImageBlock.bind(this);
  }

  addImageBlock = () => {
    this.setState({
      imageBlocks: [...this.state.imageBlocks,
      { key: uuidv4(), id: this.state.imageBlocks.length, title: 'file' + (this.state.imageBlocks.length + 1), uri: null },
    ]})
  } 

  removeImageBlock = key => { 
    this.setState({imageBlocks: this.state.imageBlocks.filter(imageBlock => imageBlock.key !== key)})
    this.setState({imageChanged: this.state.imageChanged.filter(imageBlock => imageBlock.key !== key)})
  }  

  updateData = (value, key) => {
    (this.state.imageChanged.some(imageBlock => imageBlock.key === key)) ?
      this.setState({
        imageChanged: this.state.imageChanged.map(
          imageBlock => imageBlock.key === key
            ? {...imageBlock, uri: value}
            : imageBlock
        )
      })
    : this.setState({
      imageChanged: [...this.state.imageChanged, { key: key, uri: value }]
    })
  }

  postRequest = () => {
    const imagesForm = new FormData();
    const my_data = {
      order_id: parseInt(this.props.order_id, 10),
      files: []
    };

    function buildFormData(formData, data, parentKey) {
      if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
        Object.keys(data).forEach(key => {
          buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
        });
      } else {
        const value = data == null ? '' : data;
    
        formData.append(parentKey, value);
      }
    }
    
    function jsonToFormData(data) {
      const formData = new FormData();
    
      buildFormData(formData, data);
    
      return formData;
    }

    for (const file of this.state.imageChanged) {
      if (file.uri) {
        let localUri = file.uri;
        let filename = localUri.split('/').pop();
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

      my_data.files.push({ uri: localUri, filename: filename ,name: 'files', type: type })
      } 
    }

    if (my_data.files.length) 
    fetch("http://localhost:4567/save_image", {
      body: jsonToFormData(my_data),
      method: "POST",
    })
    .catch((error) => console.error(error))
  }

  _getState =() =>  {
    console.log(this.state.imageBlocks);
  }

  render () {
    console.log(this.props);

    return (
      <Fragment>
        {/* <ImagesList
            imageBlocks={ this.props.images }
            removeImageBlock={ this.removeImageBlock }
            updateData={ this.updateData }
        />   */}

        <TouchableHighlight onPress={this.addImageBlock} style={buttonsStyles.buttonContainer}>
            <View style={buttonsStyles.button}>
              <Text style={buttonsStyles.buttonText}>Add an image</Text>
            </View>   
        </TouchableHighlight>
        <TouchableHighlight onPress={this.postRequest} style={buttonsStyles.buttonContainer}>
            <View style={buttonsStyles.button}>
              <Text style={buttonsStyles.buttonText}>Update</Text>
            </View>
        </TouchableHighlight>
      </Fragment>  
    )
  }
}

const mapStateToProps = (state) => {
  return { images: state.images }
}

export default connect(mapStateToProps)(NewList)
