import React, { useState, Component } from 'react'
import { FlatList } from 'react-native'
import ImagePickerBlock from './ImagePickerBlock'

export default class ImagesList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <FlatList
      data={ this.props.imageBlocks }
      keyExtractor={ item => item.key}
      renderItem={({ item }) => (
        <ImagePickerBlock
          title={ item.title } 
          dataKey={ item.key }
          remove={ () => this.props.removeImageBlock(item.key) }
          updateData={ this.props.updateData }
        />
      )}
    />  
    )
  }
}
