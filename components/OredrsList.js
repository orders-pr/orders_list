import React, { useEffect, useState, Fragment, Component } from 'react';
import { ActivityIndicator, FlatList, Text, StyleSheet, Frag, TouchableHighlight, View } from 'react-native';
// import ImagesUpdater from './ImageUpdater';
import ImagesListContainer from '../containers/ImagesListContainer'

export default class OredrsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: [],
      order_id: 1,
    };
  }

  _onPressItem(e, item) {
    this.setState({ order_id: parseInt(item[0], 10)})
  }

  componentDidMount() {
    fetch('http://localhost:4567/orders')
      .then((response) => response.json())
      .then((json) => this.setState({ data: json[0]}))
      // .catch((error) => console.error(error))
      .catch((error) => this.setState({ data: JSON.parse('{ "1": "Заявка #1", "2": "Заявка #2"}')}))
      .finally(() => this.setState({ isLoading: false}));
  }

  render() {
    console.log(this.state.data);
    return (
      <Fragment>
        <Text style={styles.title}>Orders</Text>
        {this.state.isLoading ? <ActivityIndicator/> : (
          <FlatList style={styles.oredrsList}
            data={Object.entries(this.state.data)}
            keyExtractor={item => item[0]}
            renderItem={({ item }) => (
              <Text style={this.state.order_id == parseInt(item[0], 10) ? styles.itemSelected : styles.item } onPress={(e) => this._onPressItem(e, item)}>{item[0]}) {item[1]}</Text>
            )}
          />
        )}

        <ImagesListContainer />
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  oredrsList: {
    maxHeight: 150,
    minWidth: 300,
    padding: 20,
    paddingTop: 0,
  },
  title: {
    textAlign: 'center',
    fontSize: 24,
    paddingTop: 15,
    paddingBottom: 15,
  },
  item: {
    color: '#000',
    padding: 5,
    fontSize: 18,
    lineHeight: 18,
    textAlign: 'center'
  },
  itemSelected: {
    color: '#2196F3',
    padding: 5,
    fontSize: 18,
    lineHeight: 18,
    textAlign: 'center'
  },
  button: {
    marginBottom: 30,
    width: 260,
    height: 50,
    alignItems: 'center',
    backgroundColor: '#2196F3',
    borderRadius: 2
  },
  buttonText: {
    textTransform: 'uppercase',
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    padding: 15,
    color: 'white'
  }
});
