import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';

import FilesList from '../components/FilesList';

import { buttonsStyles } from "../styles/button_style"

export default function FilesScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <FilesList />
      <TouchableHighlight onPress={() => navigation.navigate('Home')} style={buttonsStyles.buttonContainer}>
        <View style={buttonsStyles.button}>
          <Text style={buttonsStyles.buttonText}>Home</Text>
        </View>   
      </TouchableHighlight>   
    </View>
  );
}
