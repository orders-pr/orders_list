import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';

import { buttonsStyles } from "../styles/button_style"

export default function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <TouchableHighlight onPress={() => navigation.navigate('Orders')} style={buttonsStyles.buttonContainer}>
        <View style={buttonsStyles.button}>
          <Text style={buttonsStyles.buttonText}>Go to Orders</Text>
        </View>   
      </TouchableHighlight>
      <TouchableHighlight onPress={() => navigation.navigate('Files')} style={buttonsStyles.buttonContainer}>
        <View style={buttonsStyles.button}>
          <Text style={buttonsStyles.buttonText}>Go to Files</Text>
        </View>   
      </TouchableHighlight>
    </View>
  );
}
