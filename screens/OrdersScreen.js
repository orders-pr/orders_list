import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';

import OredrsContainer from '../containers/OrdersContainer'

import { buttonsStyles } from "../styles/button_style"

export default function OrdersScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <OredrsContainer/>
      <TouchableHighlight onPress={() => navigation.navigate('Home')} style={buttonsStyles.buttonContainer}>
        <View style={buttonsStyles.button}>
          <Text style={buttonsStyles.buttonText}>Home</Text>
        </View>   
      </TouchableHighlight>
    </View>
  );
} 
