import { StyleSheet } from 'react-native'

const buttonsStyles = StyleSheet.create({
  buttonContainer: {
    marginBottom: 5,
    alignSelf: 'stretch',
    flexDirection: 'column',
    marginHorizontal: 5,
    minHeight: 40,
  },
  button: {
    alignSelf: 'stretch',
    alignItems: 'center',
    borderRadius: 2
  },
  buttonText: {
    textTransform: 'uppercase',
    fontSize: 18,
    fontWeight: '400',
    textAlign: 'center',
    padding: 10,
    color: '#2196F3'
  }
});

export { buttonsStyles }
